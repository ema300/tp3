package tp3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

public class Intermediario extends Thread {

	private ArrayList<Persona> mejorSolucion;
	private ArrayList<Persona> listaDePersonas;
	private ArrayList<Par> listaDeIncompatibles;
	private ArrayList<Persona> auxiliarEquipo;

	int minArq;
	int maxArq;
	int minProg;
	int maxProg;
	int minTest;
	int maxTest;
	boolean flagMejorConjunto;
	boolean busquedaFinalizada;

	public Intermediario() {
		mejorSolucion = new ArrayList<Persona>();
		listaDePersonas = new ArrayList<Persona>();
		listaDeIncompatibles = new ArrayList<Par>();
		auxiliarEquipo = new ArrayList<Persona>();

		// instancias para los flags //
		minArq = maxArq = minProg = maxProg = minTest = maxTest = 0;
		flagMejorConjunto = busquedaFinalizada = false;
	}

///////////// COMUNICACION CON LA INTERFAZ ////////////////
/////////// DATOS PARA SER UTILIZADOS POR UNA LA INTERFAZ GRAFICA ///////
	public ArrayList<Persona> listaDePersonas() {
		return listaDePersonas;
	}

	public ArrayList<Par> listaDeIncompatibles() {
		return listaDeIncompatibles;
	}

	public ArrayList<Persona> dameSolucion() {
		return mejorSolucion;

	}

/////////// FLAGS PARA COMUNICAR CON LA INTERFAZ ////////////
//informa el estado del resultado
	public boolean flagMejorConjunto() {
		return flagMejorConjunto;
	}
//informa la finalizacion de las busqueda haya sido buena o mala
	public boolean busquedaFinalizada() {

		return this.busquedaFinalizada;
	}

/////////////// CARGAR LOS ARCHIVOS //////////////
//lee los archivos de las personas candidatas libres para formar un equipo
	public void cargarArchivoPersonas(File archivo) throws FileNotFoundException, IOException {
		listaDePersonas().clear();
		String cadena;

		FileReader f = new FileReader(archivo);
		BufferedReader b = new BufferedReader(f);
		while ((cadena = b.readLine()) != null) {
			agregarlistaDePersonas(cadena);

		}

		b.close();

	}
//crea una lista de personas
	private void agregarlistaDePersonas(String c) {

		int posApellido = 0;
		int posRol = 0;
		Persona auxPersona = new Persona("", "", "");

		posApellido = c.indexOf(',');
		posRol = c.indexOf(',', posApellido + 1);

		auxPersona.setNombre(c.substring(0, posApellido));
		auxPersona.setApellido(c.substring(posApellido + 1, posRol));
		auxPersona.setRol(c.substring(posRol + 1, c.length()));
		listaDePersonas.add(auxPersona);

	}
//lee las personas que alguna vez se llevaron mal trabajando
	public void cargarArchivoDeIncompatibles(File archivo) throws FileNotFoundException, IOException {
		listaDeIncompatibles().clear();
		String cadena;

		FileReader f = new FileReader(archivo);
		BufferedReader b = new BufferedReader(f);
		while ((cadena = b.readLine()) != null) {
			agregarlistaDeIncompatibles(cadena);

		}

		b.close();

	}
//crea una lista de las personas incompatibles entre si
	private void agregarlistaDeIncompatibles(String c) {

		int[] posicion = new int[5]; // posici�n de las comas que delimitan a las personas

		Persona auxPersonaUno = new Persona("", "", "");
		Persona auxPersonaDos = new Persona("", "", "");

		posicion[0] = c.indexOf(',');

		for (int i = 1; i < posicion.length; i++) {
			posicion[i] = c.indexOf(',', posicion[i - 1] + 1);
		}
		auxPersonaUno.setNombre(c.substring(0, posicion[0]));
		auxPersonaUno.setApellido(c.substring(posicion[0] + 1, posicion[1]));
		auxPersonaUno.setRol(c.substring(posicion[1] + 1, posicion[2]));
		auxPersonaDos.setNombre(c.substring(posicion[2] + 1, posicion[3]));
		auxPersonaDos.setApellido(c.substring(posicion[3] + 1, posicion[4]));
		auxPersonaDos.setRol(c.substring(posicion[4] + 1, c.length()));

		Par auxIncompatible = new Par(auxPersonaUno, auxPersonaDos);

		listaDeIncompatibles.add(auxIncompatible);//

	}

///////////// SOLUCION PARA EL USUARIO ////////////////////
//corre el hilo poniendo en marcha la busqueda de soluciones para el usuario
	@Override
	public void run() {
		this.mejorSolucion.clear();
		System.out.println("Estoy corriendo en un hilo ");
		resolver();
		busquedaFinalizada = true;
		
		System.out.println("*********Final del Hilo********");

	}

//va chequeando por cada lider una solucion hasta que el flag lo interrupta  
	private void resolver() {

		Filtro r = new Filtro();
		r.separarlideres(listaDePersonas);
		for (Persona l : r.getLideresDeProyecto()) {
			if (flagMejorConjunto == false) {
				resolverT(l, r.restoDelEquipo());
			
			}
			
				break;
			
			
		}
	}

//va capturando posibles soluciones 
	private void resolverT(Persona l, ArrayList<Persona> lista) { // resuelve por cada lider una solucion
		ArrayList<Persona> lis = new ArrayList<Persona>();
		lis.addAll(lista);
		Solucion s = new Solucion(lis, listaDeIncompatibles);
		s.agregarUnLiderParaResolver(l); // elige entre muchas combinaciones la de mayor conjunto
		System.out.println(s.dameMejorSolucionEncontrada().toString());

		requisitos(s.dameMejorSolucionEncontrada(), minArq, maxArq, minProg, maxProg, minTest, maxTest);

	}

////////////////RECIBIR REQUISITOS //////////////////////
//recibe los requisitos que pide el usuario	
	public void seterRequisitos(int minArq, int maxArq, int minProg, int maxProg, int minTest, int maxTest) {

		dameSolucion().clear();
		this.minArq = minArq;
		this.maxArq = maxArq;
		this.minProg = minProg;
		this.maxProg = maxProg;
		this.minTest = minTest;
		this.maxTest = maxTest;

	}
//por cada solucion posible chequea si es aceptable o no para el usuario
	public void requisitos(HashSet<Persona> solucion, int minArq, int maxArq, int minProg, int maxProg, int minTest,
			int maxTest) {
		Filtro r = new Filtro();
		String resultado = "";
		resultado = r.armarEquipo(solucion, minArq, maxArq, minProg, maxProg, minTest, maxTest);

		switch (resultado) {
		case "Max":
			this.mejorSolucion = r.getEquipo();
			flagMejorConjunto = true;
			auxiliarEquipo.clear();
			break;

		case "Min":
			if (r.getEquipo().size() > auxiliarEquipo.size()) {
				this.mejorSolucion = r.getEquipo();
				auxiliarEquipo = r.getEquipo();
			} else
				this.mejorSolucion = auxiliarEquipo;

			break;

		case "No":
			if (r.getEquipo().size() > auxiliarEquipo.size()) {
				this.mejorSolucion = r.getEquipo();
				auxiliarEquipo = r.getEquipo();
			} else
				this.mejorSolucion = r.getEquipo();

			break;

		}

	}

}