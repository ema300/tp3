package tp3;

import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.MatteBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Cursor;

public class SoftwareFactory {

	private JFrame frame;
	private Intermediario intermediario;
	private JTable table;
	private JTextField textLider;
	private JTextField textMinArquitecto;
	private JTextField textMaxArquitecto;
	private JTextField textMaxProgramador;
	private JTextField textMinProgramador;
	private JTextField textMaxTester;
	private JTextField textMinTester;
	private boolean flagTxtPersonas;
	private boolean flagTxtIncom;
	private boolean flagBuscarConjunto;
	private boolean flagProcesando;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SoftwareFactory window = new SoftwareFactory();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SoftwareFactory() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setTitle("Software Factory");
		frame.getContentPane().setBackground(new Color(51, 102, 153));
		frame.setBounds(100, 100, 925, 590);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		flagTxtPersonas = false;
		flagTxtIncom = false;
		flagBuscarConjunto=false;
		flagProcesando=false;
		intermediario = new Intermediario();
		table = new JTable();
		
		//////////////////TABLA //////////////

		
		table.setBackground(Color.DARK_GRAY);
		table.setForeground(Color.WHITE);
		table.setBounds(450, 24, 424, 414);
		table.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		table.setEnabled(false);
		table.setModel(
				new DefaultTableModel(
						new Object[][] { { null, null }, { null, null }, { null, null }, { null, null }, { null, null },
								{ null, null }, { null, null }, { null, null }, { null, null }, { null, null },
								{ null, null }, { null, null }, { null, null }, { null, null }, { null, null },
								{ null, null }, { null, null }, { null, null }, { null, null }, { null, null },
								{ null, null }, { null, null }, { null, null }, { null, null }, { null, null }, },
								new String[] { "", "" }));
		frame.getContentPane().add(table);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setViewportBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		scrollPane.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		scrollPane.setForeground(Color.WHITE);
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBounds(450, 24, 424, 414);
		frame.getContentPane().add(scrollPane);

		/////////////////// CARGAR LISTA DE PERSONAS /////////////////////////

		JButton btnCargarListaPersonas = new JButton("Cargar Lista de Personas");
		btnCargarListaPersonas.setBackground(new Color(0, 0, 128));
		btnCargarListaPersonas.setForeground(Color.WHITE);
		btnCargarListaPersonas.setBounds(10, 23, 333, 45);
		btnCargarListaPersonas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCargarListaPersonas.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				FileNameExtensionFilter filtro = new FileNameExtensionFilter(" *txt", "txt");
				chooser.setFileFilter(filtro);
				chooser.setApproveButtonText("Abrir .txt");
				chooser.addChoosableFileFilter(new FileFilter() {

					@Override
					public String getDescription() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public boolean accept(File f) {
						return false;
					}
				});
				if(flagProcesando==false){
					int result = chooser.showOpenDialog(null);
					if (result != JFileChooser.CANCEL_OPTION) {
						File archivo = chooser.getSelectedFile();

						try {

							flagTxtPersonas = true;
							intermediario.cargarArchivoPersonas(archivo);

						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}

				}}
		});
		frame.getContentPane().add(btnCargarListaPersonas);

		///////////////////// VER LISTA DE PERSONAS /////////////////////////

		JButton btnVerListaPersonas = new JButton("Visualizar Lista de Personas");
		btnVerListaPersonas.setBackground(new Color(0, 0, 128));
		btnVerListaPersonas.setForeground(Color.WHITE);
		btnVerListaPersonas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (flagTxtPersonas == true) {

					table.setModel(new DefaultTableModel(

							matrizAGraficar(intermediario.listaDePersonas()), new String[] { "Nombre", "Puesto" }));

				}
			}
		});
		btnVerListaPersonas.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnVerListaPersonas.setBounds(90, 87, 180, 36);
		frame.getContentPane().add(btnVerListaPersonas);

		/////////////////// CARGAR LISTA DE INCOMPATIBLILIDAD /////////////

		//archivos txt*


		JButton btnCargarIncom = new JButton("Cargar Lista de Incompatibilidad");
		btnCargarIncom.setBackground(new Color(0, 0, 128));
		btnCargarIncom.setForeground(Color.WHITE);
		btnCargarIncom.setBounds(10, 142, 333, 45);
		btnCargarIncom.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCargarIncom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
				FileNameExtensionFilter filtro = new FileNameExtensionFilter(" *txt", "txt");
				chooser.setFileFilter(filtro);
				chooser.setApproveButtonText("Abrir .txt");
				chooser.addChoosableFileFilter(new FileFilter() {

					@Override
					public String getDescription() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public boolean accept(File f) {
						return false;
					}
				});
				if(flagProcesando==false){
					int result = chooser.showOpenDialog(null);
					if (result != JFileChooser.CANCEL_OPTION) {
						File archivo = chooser.getSelectedFile();

						try {
							flagTxtIncom = true;
							table.removeAll();
							intermediario.cargarArchivoDeIncompatibles(archivo);

						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

					}
				}}

		});
		frame.getContentPane().add(btnCargarIncom);

		//////////////////VER LISTA DE INCOMPATIBILIDAd /////////////////

		JButton btnVerListaIncom = new JButton("Visualizar Lista de Incompatibilidad");
		btnVerListaIncom.setBackground(new Color(0, 0, 128));
		btnVerListaIncom.setForeground(Color.WHITE);
		btnVerListaIncom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (flagTxtIncom == true) {
					table.setModel(new DefaultTableModel(

							matrizAGraficarDos(intermediario.listaDeIncompatibles()),
							new String[] { " Incompatibles", "" }));

				}

			}
		});
		btnVerListaIncom.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnVerListaIncom.setBounds(61, 198, 234, 36);
		frame.getContentPane().add(btnVerListaIncom);

		////////////////INGRESOS PARA EL USUARIO ////////////////////

		//Seleccionar los requerimientos para el conjunto 


		JLabel lblSeleccioneLosRequerimientos = new JLabel("Ingrese los Requerimientos deseados:");
		lblSeleccioneLosRequerimientos.setForeground(Color.WHITE);
		lblSeleccioneLosRequerimientos.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSeleccioneLosRequerimientos.setBounds(10, 258, 333, 20);
		frame.getContentPane().add(lblSeleccioneLosRequerimientos);

		JLabel lblLider = new JLabel("Lider ");
		lblLider.setForeground(Color.WHITE);
		lblLider.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblLider.setBounds(10, 289, 54, 20);
		frame.getContentPane().add(lblLider);

		JLabel lblArquitectos = new JLabel("Arquitectos");
		lblArquitectos.setForeground(Color.WHITE);
		lblArquitectos.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblArquitectos.setBounds(76, 289, 83, 20);
		frame.getContentPane().add(lblArquitectos);

		JLabel lblProgramadores = new JLabel("Programadores ");
		lblProgramadores.setForeground(Color.WHITE);
		lblProgramadores.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblProgramadores.setBounds(208, 289, 114, 20);
		frame.getContentPane().add(lblProgramadores);

		JLabel lblTesters = new JLabel("Testers");
		lblTesters.setForeground(Color.WHITE);
		lblTesters.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTesters.setBounds(332, 289, 54, 20);
		frame.getContentPane().add(lblTesters);

		JLabel lblMinArq = new JLabel("M�nimo");
		lblMinArq.setForeground(Color.WHITE);
		lblMinArq.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMinArq.setBounds(79, 318, 54, 20);
		frame.getContentPane().add(lblMinArq);

		JLabel lblMaxArq = new JLabel("M�ximo");
		lblMaxArq.setForeground(Color.WHITE);
		lblMaxArq.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaxArq.setBounds(79, 357, 54, 20);
		frame.getContentPane().add(lblMaxArq);

		JLabel lblMinProg = new JLabel("M�nimo");
		lblMinProg.setForeground(Color.WHITE);
		lblMinProg.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMinProg.setBounds(208, 320, 54, 20);
		frame.getContentPane().add(lblMinProg);

		JLabel lblMaxProg = new JLabel("M�ximo");
		lblMaxProg.setForeground(Color.WHITE);
		lblMaxProg.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaxProg.setBounds(208, 359, 54, 20);
		frame.getContentPane().add(lblMaxProg);

		JLabel lblMinTest = new JLabel("M�nimo");
		lblMinTest.setForeground(Color.WHITE);
		lblMinTest.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMinTest.setBounds(330, 320, 54, 20);
		frame.getContentPane().add(lblMinTest);

		JLabel lblMaxTest = new JLabel("M�ximo");
		lblMaxTest.setForeground(Color.WHITE);
		lblMaxTest.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMaxTest.setBounds(330, 359, 54, 20);
		frame.getContentPane().add(lblMaxTest);

		textLider = new JTextField();
		textLider.setForeground(Color.BLACK);
		textLider.setEditable(false);
		textLider.setText("1");
		textLider.setBounds(10, 320, 44, 20);
		frame.getContentPane().add(textLider);
		textLider.setColumns(10);

		textMinArquitecto = new JTextField();
		textMinArquitecto.setForeground(Color.BLACK);
		textMinArquitecto.setColumns(10);
		textMinArquitecto.setBounds(131, 320, 44, 20);
		frame.getContentPane().add(textMinArquitecto);

		textMaxArquitecto = new JTextField();
		textMaxArquitecto.setForeground(Color.BLACK);
		textMaxArquitecto.setColumns(10);
		textMaxArquitecto.setBounds(131, 359, 44, 20);
		frame.getContentPane().add(textMaxArquitecto);

		textMinProgramador = new JTextField();
		textMinProgramador.setForeground(Color.BLACK);
		textMinProgramador.setColumns(10);
		textMinProgramador.setBounds(260, 322, 44, 20);
		frame.getContentPane().add(textMinProgramador);

		textMaxProgramador = new JTextField();
		textMaxProgramador.setForeground(Color.BLACK);
		textMaxProgramador.setColumns(10);
		textMaxProgramador.setBounds(260, 361, 44, 20);
		frame.getContentPane().add(textMaxProgramador);

		textMinTester = new JTextField();
		textMinTester.setForeground(Color.BLACK);
		textMinTester.setColumns(10);
		textMinTester.setBounds(382, 322, 44, 20);
		frame.getContentPane().add(textMinTester);

		textMaxTester = new JTextField();
		textMaxTester.setForeground(Color.BLACK);
		textMaxTester.setColumns(10);
		textMaxTester.setBounds(382, 361, 44, 20);
		frame.getContentPane().add(textMaxTester);

		//////////////INICIAR LA BUSQUEDA DEL USUARIO ////////////////////////

		//Buscar la mejor soluci�n acorde a los requerimientos

		JButton btnBuscarConjunto = new JButton("Buscar Mejor Conjunto");
		btnBuscarConjunto.setBackground(new Color(0, 0, 128));
		btnBuscarConjunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (flagProcesando==false && flagTxtPersonas == true && flagTxtIncom == true) {
					if (sonNumerosValidos() == true) {

						int minArq = Integer.parseInt(textMinArquitecto.getText());
						int maxArq = Integer.parseInt(textMaxArquitecto.getText());

						int minProg = Integer.parseInt(textMinProgramador.getText());
						int maxProg = Integer.parseInt(textMaxProgramador.getText());

						int minTest = Integer.parseInt(textMinTester.getText());
						int maxTest = Integer.parseInt(textMaxTester.getText());
     
						intermediario.start();
						
						intermediario.seterRequisitos(minArq, maxArq, minProg, maxProg, minTest, maxTest);
						flagProcesando=true;
						flagBuscarConjunto=true;


					} else
						JOptionPane.showMessageDialog(null, "No son n�meros validos");
				}
			}
		});
		btnBuscarConjunto.setForeground(Color.WHITE);
		btnBuscarConjunto.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnBuscarConjunto.setBounds(100, 426, 168, 30);
		frame.getContentPane().add(btnBuscarConjunto);

		/////////////////// VISUALIZAR MEJOR SOLUCION/////////////////////////

		JButton btnVerConjunto = new JButton("Visualizar Conjunto Encontrado");
		btnVerConjunto.setBackground(new Color(0, 0, 128));
		btnVerConjunto.setForeground(Color.WHITE);
		btnVerConjunto.setBounds(10, 480, 333, 45);
		btnVerConjunto.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnVerConjunto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (flagBuscarConjunto==true){
					if (intermediario.flagMejorConjunto()== true || intermediario.busquedaFinalizada()==true) {

						flagProcesando=false;
					

						if (intermediario.dameSolucion().size() != 0) {

							if(intermediario.flagMejorConjunto()== false && intermediario.busquedaFinalizada()==true){

								JOptionPane.showMessageDialog(null, "El equipo encontrado no es el m�ximo pedido");}


							table.setModel(new DefaultTableModel(

									matrizAGraficar(intermediario.dameSolucion()), new String[] { "Nombre", "Puesto" }));


						}
						else {
							JOptionPane.showMessageDialog(null, "No se pudo formar el equipo con la cantidad m�nima");

						}

					}

					else {
						JOptionPane.showMessageDialog(null, "Busqueda en proceso");

					}
				}}
		});
		frame.getContentPane().add(btnVerConjunto);

	}

	////////////////PREPARACION PARA GRAFICAR ////////////
	//Genera una matriz con los datos una lista de personas para mostrarlos en
	//una tabla

	public String[][] matrizAGraficar(ArrayList<Persona> lista) {

		String m[][] = new String[lista.size()][2];

		for (int i = 0; i < lista.size(); i++) {

			Persona c = new Persona("", "", "");
			c = lista.get(i);

			m[i][0] = c.getNombre() + " " + c.getApellido();
			m[i][1] = c.getRol();

		}
		return m;
	}
	
	//graficar la lista de incompatibles
	public String[][] matrizAGraficarDos(ArrayList<Par> lista) {

		String m[][] = new String[lista.size()][2];

		for (int i = 0; i < lista.size(); i++) {

			Par par = new Par(null, null);
			par = lista.get(i);

			m[i][0] = par.getUno().getNombre() + " " + par.getUno().getApellido() + " (" + par.getUno().getRol() + ")";

			m[i][1] = par.getDos().getNombre() + " " + par.getDos().getApellido() + " (" + par.getDos().getRol() + ")";
		}
		return m;
	}
	//////////////VALIDACION DE INGRESOS ////////////////////

	//Para utilizar al ingresar arquitectos, programadores y testers.
	private boolean esNumero(String i) {
		try {
			Integer.parseUnsignedInt(i);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	// Chequeo para que sean datos validos
	private boolean sonNumerosValidos() {
		boolean ret = true;

		ret = ret && esNumero(textMinArquitecto.getText()) && esNumero(textMaxArquitecto.getText())
				&& esNumero(textMinProgramador.getText()) && esNumero(textMaxProgramador.getText())
				&& esNumero(textMinTester.getText()) && esNumero(textMaxTester.getText());

		return ret;
	}


}
