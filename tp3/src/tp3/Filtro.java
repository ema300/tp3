package tp3;

import java.util.ArrayList;
import java.util.HashSet;

public class Filtro {
	private ArrayList<Persona> lideresDeProyecto;
	private ArrayList<Persona> arquitectos;
	private ArrayList<Persona> programadores;
	private ArrayList<Persona> testers;
	private ArrayList<Persona> Equipo;

	private ArrayList<Persona> restoDelEquipo;

	public Filtro() {
		this.lideresDeProyecto = new ArrayList<Persona>();
		this.arquitectos = new ArrayList<Persona>();
		this.programadores = new ArrayList<Persona>();
		this.testers = new ArrayList<Persona>();
		this.Equipo = new ArrayList<Persona>();
		this.restoDelEquipo = new ArrayList<Persona>();
	}

//////////////// SEPARAR EN GRUPOS ///////////
//separa en grupos dada una lista
	public void separarPorRol(HashSet<Persona> lista) {

		for (Persona p : lista) {

			if (p.getRol().equals("lider")) {
				lideresDeProyecto.add(p);
			}
			if (p.getRol().equals("arquitecto")) {
				arquitectos.add(p);
			}
			if (p.getRol().equals("programador")) {
				programadores.add(p);
			}
			if (p.getRol().equals("tester")) {
				testers.add(p);
			}
		}
	}

// separa el lider de los demas roles
	public void separarlideres(ArrayList<Persona> lista) {
		for (Persona p : lista) {

			if (p.getRol().equals("lider")) {
				lideresDeProyecto.add(p);
			} else {
				restoDelEquipo.add(p);

			}
		}
	}

//////////////////// DAR UNA SOLUCION ////////////////
//por cada solucion notifica si hay una solucion candidata o no
	public String armarEquipo(HashSet<Persona> solucion, int minArq, int maxArq, int minProg, int maxProg, int minTest,
			int maxTest) {

		int auxMinArq, auxMinTest, auxMinProg;
		auxMinArq = auxMinTest = auxMinProg = 0;

		separarPorRol(solucion);

		if (verificarRequisitosMax(maxArq, maxProg, maxTest) == true) {
			System.out.println("Requisitos maximos cumplidos");
			ArmarListaDelEquipo(maxArq, maxProg, maxTest);
			return "Max";
		} else if (verificarRequisitosMin(minArq, minProg, minTest) == true) {
			System.out.println("Requisitos minimos cumplidos");
			if (arquitectos.size() <= maxArq) {
				auxMinArq = arquitectos.size();
			} else {
				auxMinArq = maxArq;
			}

			if (testers.size() <= maxTest) {
				auxMinTest = testers.size();
			} else {
				auxMinTest = maxTest;
			}

			if (programadores.size() <= maxProg) {
				auxMinProg = programadores.size();
			} else {
				auxMinProg = maxProg;
			}

			ArmarListaDelEquipo(auxMinArq, auxMinProg, auxMinTest);
			return "Min";
		} else {
			ArmarListaDelEquipo(arquitectos.size(), programadores.size(), testers.size());
			this.Equipo.clear();
			System.out.println("Requisitos NO cumplidos");
			return "No";
		}

	}

//arma la cantidad posible si es posible generar una solucion para el usuario 
	public void ArmarListaDelEquipo(int cantArq, int cantProg, int cantTest) {

		Persona auxPersona = new Persona("", "", "");

		auxPersona = lideresDeProyecto.get(0);
		Equipo.add(auxPersona);

		int i = 0;
		while (i < cantArq) {
			auxPersona = arquitectos.get(i);
			Equipo.add(auxPersona);
			i++;
		}

		for (int j = 0; j < cantTest; j++) {
			auxPersona = testers.get(j);
			Equipo.add(auxPersona);
		}

		for (int k = 0; k < cantProg; k++) {
			auxPersona = programadores.get(k);
			Equipo.add(auxPersona);
		}

		System.out.println("Equipo final");
		System.out.println(Equipo.toString());
		System.out.println("-----------------------------------------------------------------------");

	}

//////////////// METODOS DE VERIFCACION PARA DAR UNA SOLUCION ////////////////77
//verifica que se cumplan los requisitos minimos
	private boolean verificarRequisitosMin(int minArq, int minProg, int minTest) {

		boolean ReqArqOk, ReqTestOk, ReqProgOk;

		ReqArqOk = ReqTestOk = ReqProgOk = false;

		if (arquitectos.size() >= minArq)
			ReqArqOk = true;

		if (testers.size() >= minTest)
			ReqTestOk = true;

		if (programadores.size() >= minProg)
			ReqProgOk = true;

		return ReqArqOk && ReqTestOk && ReqProgOk;

	}

//verifica que se cumplan los requisitos maximos 
	private boolean verificarRequisitosMax(int maxArq, int maxProg, int maxTest) {

		boolean ReqMaxArqOk, ReqMaxTestOk, ReqMaxProgOk;

		ReqMaxArqOk = ReqMaxTestOk = ReqMaxProgOk = false;

		if (arquitectos.size() >= maxArq)
			ReqMaxArqOk = true;

		if (testers.size() >= maxTest)
			ReqMaxTestOk = true;

		if (programadores.size() >= maxProg)
			ReqMaxProgOk = true;

		return ReqMaxArqOk && ReqMaxTestOk && ReqMaxProgOk;

	}

/////////////////// GETERS ///////////////////////
	public ArrayList<Persona> getEquipo() {
		return Equipo;
	}

	public ArrayList<Persona> getLideresDeProyecto() {
		return lideresDeProyecto;
	}

	public ArrayList<Persona> getArquitectos() {
		return arquitectos;
	}

	public ArrayList<Persona> getProgramadores() {
		return programadores;
	}

	public ArrayList<Persona> getTesters() {
		return testers;
	}

	public ArrayList<Persona> getListaDelEquipo() {
		return Equipo;
	}

	public ArrayList<Persona> restoDelEquipo() {
		return restoDelEquipo;
	}

}
