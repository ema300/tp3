package tp3;


public class Par {
	

	private Persona uno;
	private Persona dos;
	public Par(Persona p1, Persona p2) {
		
		uno=p1;
		dos=p2;
		
		
	}
	public Persona getUno() {
		return uno;
	}
	
	public Persona getDos() {
		return dos;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dos == null) ? 0 : dos.hashCode());
		result = prime * result + ((uno == null) ? 0 : uno.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		
		Par other = (Par) obj;
		if (dos == null) {
			if (other.dos != null)
				return false;
		} else if (!dos.equals(other.dos))
			return false;
		if (uno == null) {
			if (other.uno != null)
				return false;
		} else if (!uno.equals(other.uno))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Par [uno=" + uno.toString() + ", dos=" + dos.toString() + "]";
	}
	
	
	

}
