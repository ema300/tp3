package tp3;


import java.util.ArrayList;
import java.util.HashSet;


public class Solucion  {
	private ArrayList<Persona> listaDePersonas;
	private ArrayList<Par> listaDeIncompatibles;
	private HashSet<Persona> conjuntoActual;
	private HashSet<Persona> candidatos;

	public Solucion(ArrayList<Persona> listaDePersonas, ArrayList<Par> listaDeIncompatibles) {
		this.listaDePersonas = listaDePersonas;
		this.listaDeIncompatibles = listaDeIncompatibles;
		
        //instancia para la recursion 
		conjuntoActual = new HashSet<Persona>();
		candidatos = new HashSet<Persona>();
	}

//////////////////////METODOS PRINCIPALES/////////////////	
// agrega un lider para ir armando posibles combinaciones con el
	public void agregarUnLiderParaResolver(Persona l) {
		
		conjuntoActual.add(l);
		Resolver();
	}
// llama a la recursion para hacer las posibles combinaciones
	private void Resolver(){

		int cont =0;	    
		recursion( listaDePersonas, cont);

	}
//va formando diferentes conjuntos de combinaciones
	private void recursion( ArrayList<Persona> lista, int cont) {
		// Caso base
		if (cont == lista.size()) {
			if (conjuntoActual.size() >= candidatos.size()) {
				candidatos = clonarConjuntoActual(conjuntoActual);
			}
			return;	

		}

		// Caso recursivo

		agregarAlConjunto(lista.get(cont));

		recursion( lista, cont + 1);

		conjuntoActual.remove(lista.get(cont));

		recursion( lista, cont +1);

	}
// chequea antes de formar una posible combinacion que no sea incompatible 

/////////////////////METODOS PARA LA RECURSION//////////////////////////////
//	con los del conjunto que va armando
	private void agregarAlConjunto(Persona p) {
		if (esCompatibleConConjuntoActual(p)) {
			conjuntoActual.add(p);
		}

	}
//clona el conjunto
	private HashSet<Persona> clonarConjuntoActual(HashSet<Persona> clonar) {
		HashSet<Persona> ret = new HashSet<Persona>();
		for (Persona i : clonar) {
			ret.add(i);
		}
		return ret;
	}
//returna verdadero o falso si la persona esta o no esta en la "listaDeIncompatible"
	private boolean sonCompatibles(Persona p, Persona o) {

		Par par = new Par(p,o);

		if (!listaDeIncompatibles.contains(par)) {
			return true;
		} else {
			return false;
		}

	}
//retorna verdadero o falso si la persona es compatible 
//con todos dentro del conjunto
	private boolean esCompatibleConConjuntoActual(Persona p) {
		boolean ret = true;
		for (Persona c : conjuntoActual) {

			ret = ret && sonCompatibles(c, p);
		}
		return ret;
	}
// retorna la combinacion mas grande que se pudo hacer
	public HashSet<Persona> dameMejorSolucionEncontrada() {
	return candidatos;
	}

	///////////////////////////GETERS//////////////////////
	public ArrayList<Persona> getListaDePersonas() {
		return listaDePersonas;
	}
	public ArrayList<Par> getListaDeIncompatibles() {
		return listaDeIncompatibles;
	}
	public HashSet<Persona> getConjuntoActual() {
		return conjuntoActual;
	}

}
