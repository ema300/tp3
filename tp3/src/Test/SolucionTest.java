package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import tp3.Par;
import tp3.Persona;
import tp3.Solucion;

public class SolucionTest {

	ArrayList<Persona> listaSinLideres;
	ArrayList<Par> listaDeIncompatibles;


	@Before
	public void setUp() throws Exception {
		
		listaSinLideres=new ArrayList<Persona>() ;
		listaDeIncompatibles=new ArrayList<Par>() ;

		Persona p1=new Persona("Noemi", "Cejas", "programador");
		Persona p2=new Persona("Carlos", "Urza", "programador");
		Persona p3=new Persona("Roman", "Martinez", "arquitecto");
		Persona p4=new Persona("Grabriela", "Galvan", "tester");

		listaSinLideres.add(p1);
		listaSinLideres.add(p2);
		listaSinLideres.add(p3);
		listaSinLideres.add(p4);

		Par incompatibles1= new Par(p2,p3);
		Par incompatibles2= new Par(p2,p4);

		listaDeIncompatibles.add(incompatibles1);
		listaDeIncompatibles.add(incompatibles2);
	}

	@Test
	public void noSonCompatibles(){

		Persona lider0=new Persona("Ariel", "Bregman", "lider");
		Persona per0=new Persona("Carlos", "Urza", "programador");
		Persona per1=new Persona("Roman", "Martinez", "arquitecto");

		Par incompatibles= new Par(per0,per1);

		Solucion solucion= new Solucion(listaSinLideres, listaDeIncompatibles);
		solucion.agregarUnLiderParaResolver(lider0);

		boolean ret= solucion.getListaDeIncompatibles().contains(incompatibles);

		assertTrue(ret);

	}

	@Test
	public void sonCompatibles(){

		Persona lider0=new Persona("Ariel", "Bregman", "lider");
		Persona per0=new Persona("Noemi", "Cejas", "programador");
		Persona per1=new Persona("Grabriela", "Galvan", "tester");

		Par compatibles= new Par(per0,per1);

		Solucion solucion= new Solucion(listaSinLideres, listaDeIncompatibles);
		solucion.agregarUnLiderParaResolver(lider0);

		boolean ret= solucion.getListaDeIncompatibles().contains(compatibles);

		assertFalse(ret);

	}

	@Test
	public void tamanioConjuntoCorrecto() {

		Solucion solucion= new Solucion(listaSinLideres, listaDeIncompatibles);

		Persona lider0=new Persona("Ariel", "Bregman", "lider");

		solucion.agregarUnLiderParaResolver(lider0);

		int n=solucion.dameMejorSolucionEncontrada().size();
		
		
		assertEquals(4, n ,0);
	}

	@Test
	public void conjuntoCompatibleCorrecto() {

		Solucion solucion= new Solucion(listaSinLideres, listaDeIncompatibles);

		Persona lider0=new Persona("Ariel", "Bregman", "lider");
		Persona p1=new Persona("Noemi", "Cejas", "programador");
		Persona p2=new Persona("Carlos", "Urza", "programador");
		Persona p3=new Persona("Roman", "Martinez", "arquitecto");
		Persona p4=new Persona("Grabriela", "Galvan", "tester");

		solucion.agregarUnLiderParaResolver(lider0);

		assertTrue(solucion.dameMejorSolucionEncontrada().contains(p1));
		assertTrue(solucion.dameMejorSolucionEncontrada().contains(p3));
		assertTrue(solucion.dameMejorSolucionEncontrada().contains(p4));
		assertFalse(solucion.dameMejorSolucionEncontrada().contains(p2));
	}


	@Test
	public void solucionConListaVacia() {

		ArrayList<Persona> listaSinLideresVacia=new ArrayList<Persona>() ;
		ArrayList<Par> listaDeIncompatiblesVacia=new ArrayList<Par>() ;

		Solucion solucion= new Solucion(listaSinLideresVacia, listaDeIncompatiblesVacia);

		Persona lider0=new Persona("Ariel", "Bregman", "lider");

		solucion.agregarUnLiderParaResolver(lider0);

		solucion.dameMejorSolucionEncontrada().size();

		int n=solucion.dameMejorSolucionEncontrada().size();

		assertEquals(1, n, 0);
	}

	@Test
	public void liderSinCompatibles() {

		ArrayList<Persona> listaSinLideres=new ArrayList<Persona>() ;
		ArrayList<Par> listaDeIncompatibles=new ArrayList<Par>() ;

		Persona lider0=new Persona("Ariel", "Bregman", "lider");
		Persona p1=new Persona("Noemi", "Cejas", "programador");
		Persona p2=new Persona("Carlos", "Urza", "programador");
		Persona p3=new Persona("Roman", "Martinez", "arquitecto");

		listaSinLideres.add(p1);
		listaSinLideres.add(p2);
		listaSinLideres.add(p3);


		Par incompatibles1= new Par(lider0,p1);
		Par incompatibles2= new Par(lider0,p2);
		Par incompatibles3= new Par(lider0,p3);

		listaDeIncompatibles.add(incompatibles1);
		listaDeIncompatibles.add(incompatibles2);
		listaDeIncompatibles.add(incompatibles3);

		Solucion solucion= new Solucion(listaSinLideres, listaDeIncompatibles);

		solucion.agregarUnLiderParaResolver(lider0);

		int n=solucion.dameMejorSolucionEncontrada().size();

		assertEquals(1, n, 0);
	}


}

