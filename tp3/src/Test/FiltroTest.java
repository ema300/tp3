package Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import tp3.Persona;
import tp3.Filtro;

public class FiltroTest {

	Filtro requisitos;
	ArrayList<Persona> lista;
	HashSet<Persona> conjuntoAFiltrar;

	@Before
	public void setUp() throws Exception {

		requisitos= new Filtro();
		lista=new ArrayList<Persona>() ;
		conjuntoAFiltrar=new HashSet<Persona>();

		Persona lider=new Persona("Ariel", "Bregman", "lider");
		Persona p1=new Persona("Roman", "Martinez", "arquitecto");
		Persona p2=new Persona("Belen", "Alonso", "arquitecto");
		Persona p3=new Persona("Celeste", "Gonzalez", "arquitecto");
		Persona p4=new Persona("Franco", "Marcico", "arquitecto");
		Persona p5=new Persona("Noemi", "Cejas", "programador");
		Persona p6=new Persona("Carlos", "Urza", "programador");
		Persona p7=new Persona("Ignacio", "Scocco", "programador");
		Persona p8=new Persona("Juan", "Quintero", "programador");
		Persona p9=new Persona("Matias", "Suarez", "tester");
		Persona p10=new Persona("Grabriela", "Galvan", "tester");
		Persona p11=new Persona("Andre", "Siciliani", "tester");
		Persona p12=new Persona("Micaela", "Vargas", "tester");

		lista.add(lider);
		lista.add(p1);		
		lista.add(p2);
		lista.add(p3);
		lista.add(p4);
		lista.add(p5);
		lista.add(p6);
		lista.add(p7);
		lista.add(p8);
		lista.add(p9);
		lista.add(p10);
		lista.add(p11);
		lista.add(p12);

		conjuntoAFiltrar.addAll(lista);

	}
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}


	@Test
	public void maximoTresPorRolTamanio() {
		requisitos.armarEquipo(conjuntoAFiltrar, 1, 3,1, 3, 1, 3);
		int n=requisitos.getEquipo().size();


		assertEquals(10, n ,0);

	}

	@Test
	public void ceroEnTodosLosRequisitosTamanio() {
		requisitos.armarEquipo(conjuntoAFiltrar, 0, 0,0, 0, 0, 0);
		int n=requisitos.getEquipo().size();


		assertEquals(1, n ,0);

	}

	@Test
	public void maxMinIgualesTamanio() {
		requisitos.armarEquipo(conjuntoAFiltrar, 4, 4,4, 4, 4, 4);
		int n=requisitos.getEquipo().size();


		assertEquals(13, n ,0);

	}

	@Test
	public void requisitosSinCumplirTamanio() {
		requisitos.armarEquipo(conjuntoAFiltrar, 5, 5,5, 5, 5, 5);
		int n=requisitos.getEquipo().size();

		assertEquals(0, n ,0);

	}

	@Test
	public void equipoSoloConArquitectosTamanio() {
		requisitos.armarEquipo(conjuntoAFiltrar, 1, 3,0, 0, 0, 0);
		int n=requisitos.getEquipo().size();

		assertEquals(4, n ,0);

	}

	@Test
	public void equipoSoloConProgramadoresTamanio() {
		requisitos.armarEquipo(conjuntoAFiltrar, 0, 0,1, 1, 0, 0);
		int n=requisitos.getEquipo().size();

		assertEquals(2, n ,0);

	}

	@Test
	public void equipoSoloConTestersTamanio() {
		requisitos.armarEquipo(conjuntoAFiltrar, 0, 0,0, 0, 4, 4);
		int n=requisitos.getEquipo().size();

		assertEquals(5, n ,0);

	}

	@Test
	public void verificarEquipoCompleto() {
		requisitos.armarEquipo(conjuntoAFiltrar, 4, 4,4, 4, 4, 4);

		Persona lider=new Persona("Ariel", "Bregman", "lider");
		Persona p1=new Persona("Roman", "Martinez", "arquitecto");
		Persona p2=new Persona("Belen", "Alonso", "arquitecto");
		Persona p3=new Persona("Celeste", "Gonzalez", "arquitecto");
		Persona p4=new Persona("Franco", "Marcico", "arquitecto");
		Persona p5=new Persona("Noemi", "Cejas", "programador");
		Persona p6=new Persona("Carlos", "Urza", "programador");
		Persona p7=new Persona("Ignacio", "Scocco", "programador");
		Persona p8=new Persona("Juan", "Quintero", "programador");
		Persona p9=new Persona("Matias", "Suarez", "tester");
		Persona p10=new Persona("Grabriela", "Galvan", "tester");
		Persona p11=new Persona("Andre", "Siciliani", "tester");
		Persona p12=new Persona("Micaela", "Vargas", "tester");

		assertTrue(requisitos.getEquipo().contains(lider));
		assertTrue(requisitos.getEquipo().contains(p1));
		assertTrue(requisitos.getEquipo().contains(p2));
		assertTrue(requisitos.getEquipo().contains(p3));
		assertTrue(requisitos.getEquipo().contains(p4));
		assertTrue(requisitos.getEquipo().contains(p5));
		assertTrue(requisitos.getEquipo().contains(p6));
		assertTrue(requisitos.getEquipo().contains(p7));
		assertTrue(requisitos.getEquipo().contains(p8));
		assertTrue(requisitos.getEquipo().contains(p9));
		assertTrue(requisitos.getEquipo().contains(p10));
		assertTrue(requisitos.getEquipo().contains(p11));
		assertTrue(requisitos.getEquipo().contains(p12));


	}

	@Test
	public void verificarEquipoIncompleto() {
		requisitos.armarEquipo(conjuntoAFiltrar, 4, 4,0, 0, 0, 0);

		Persona lider=new Persona("Ariel", "Bregman", "lider");
		Persona p1=new Persona("Roman", "Martinez", "arquitecto");
		Persona p2=new Persona("Belen", "Alonso", "arquitecto");
		Persona p3=new Persona("Celeste", "Gonzalez", "arquitecto");
		Persona p4=new Persona("Franco", "Marcico", "arquitecto");
		Persona p5=new Persona("Noemi", "Cejas", "programador");
		Persona p6=new Persona("Carlos", "Urza", "programador");
		Persona p7=new Persona("Ignacio", "Scocco", "programador");
		Persona p8=new Persona("Juan", "Quintero", "programador");
		Persona p9=new Persona("Matias", "Suarez", "tester");
		Persona p10=new Persona("Grabriela", "Galvan", "tester");
		Persona p11=new Persona("Andre", "Siciliani", "tester");
		Persona p12=new Persona("Micaela", "Vargas", "tester");

		assertTrue(requisitos.getEquipo().contains(lider));
		assertTrue(requisitos.getEquipo().contains(p1));
		assertTrue(requisitos.getEquipo().contains(p2));
		assertTrue(requisitos.getEquipo().contains(p3));
		assertTrue(requisitos.getEquipo().contains(p4));
		assertFalse(requisitos.getEquipo().contains(p5));
		assertFalse(requisitos.getEquipo().contains(p6));
		assertFalse(requisitos.getEquipo().contains(p7));
		assertFalse(requisitos.getEquipo().contains(p8));
		assertFalse(requisitos.getEquipo().contains(p9));
		assertFalse(requisitos.getEquipo().contains(p10));
		assertFalse(requisitos.getEquipo().contains(p11));
		assertFalse(requisitos.getEquipo().contains(p12));


	}

}
